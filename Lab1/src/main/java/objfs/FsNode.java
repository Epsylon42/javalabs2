package objfs;

import java.util.List;
import java.util.Arrays;
import java.util.Collections;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;


public abstract class FsNode {
    ReadWriteLock rwlock = new ReentrantReadWriteLock();
    Directory parent;

    public Directory getParent() {
        return readonly(() -> parent);
    }

    public void moveTo(String path) {
        List<String> chunks = splitPath(path);
        if (chunks.size() == 1) {
            String newName = chunks.get(0);
            Directory p = getParent();
            FsNode target = p.getByPath(newName);
            p.take(this);
            if (target != null && target instanceof Directory) {
                target.asDir().put(newName, this);
            } else {
                p.put(newName, this);
            }
        } else {
            String newName = chunks.get(0);
            chunks.remove(0);
            Directory p = getParent();
            Directory destination = p.getByPathChunks(chunks).asDir();
            parent.take(this);
            destination.put(newName, this);
        }
    }

    protected <T> T readonly(Supplier<T> func) {
        rwlock.readLock().lock();
        T ret = func.get();
        rwlock.readLock().unlock();
        return ret;
    }

    protected void readonly(Callback func) {
        rwlock.readLock().lock();
        func.call();
        rwlock.readLock().unlock();
    }

    protected <T> T readwrite(Supplier<T> func) {
        rwlock.writeLock().lock();
        T ret = func.get();
        rwlock.writeLock().unlock();
        return ret;
    }

    protected void readwrite(Callback func) {
        rwlock.writeLock().lock();
        func.call();
        rwlock.writeLock().unlock();
    }

    protected static List<String> splitPath(String path) {
        List<String> chunks = Arrays.stream(path.split("/")).collect(Collectors.toList());
        Collections.reverse(chunks);
        return chunks;
    }

    public <T extends FsNode> T cast() {
        return (T)this;
    }

    public Directory asDir() {
        return (Directory)this;
    }

    protected String debugPrint() {
        return getClass().getName();
    }

    public String show(int depth) {
        String p = "";
        for (int i = 0; i < depth; i++) {
            p += "  ";
        }
        final String prefix = p;

        return Arrays.stream(debugPrint().split("\n"))
            .map(line -> prefix + line)
            .collect(Collectors.joining("\n"));
    }
}
