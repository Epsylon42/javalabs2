package objfs;

import java.util.List;
import java.util.LinkedList;
import java.util.concurrent.locks.Condition;


public class Fifo<T> extends FsNode {
    List<T> queue;
    Condition notEmpty;

    public Fifo() {
        queue = new LinkedList<>();
        notEmpty = rwlock.writeLock().newCondition();
    }

    public void write(T elem) {
        readwrite(() -> {
            queue.add(elem);
            notEmpty.signal();
        });
    }

    public T try_read() {
        return readwrite(() -> {
            if (queue.isEmpty()) {
                return null;
            } else {
                return queue.remove(0);
            }
        });
    }

    public T wait_read() throws InterruptedException {
        rwlock.writeLock().lock();
        while (queue.isEmpty()) {
            notEmpty.await();
        }

        T ret = queue.remove(0);
        rwlock.writeLock().unlock();
        return ret;
    }
}
