package objfs;


public class Pair<T, U> {
    public T fst;
    public U snd;

    public Pair(T fst, U snd) {
        this.fst = fst;
        this.snd = snd;
    }
}
