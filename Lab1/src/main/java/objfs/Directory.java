package objfs;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.ArrayList;
import java.util.HashMap;

public class Directory extends FsNode {
    HashMap<String, FsNode> contents = new HashMap<>();

    public void put(String key, FsNode value) {
        readwrite(() -> {
            contents.put(key, value);
            value.parent = this;
        });
    }

    public FsNode get(String key) {
        return readonly(() -> contents.get(key));
    }

    public List<Pair<String, FsNode>> list() {
        List<Pair<String, FsNode>> elems = new ArrayList<>();
        readonly(() -> contents.forEach((k, v) -> elems.add(new Pair<>(k, v))));
        return elems;
    }

    public Directory rootDirectory() {
        Directory p = getParent();
        if (p == null) {
            return this;
        } else {
            return p.rootDirectory();
        }
    }

    public FsNode take(String name) {
        return readwrite(() -> {
            FsNode elem = contents.get(name);
            if (elem != null) {
                if (elem.parent == this) {
                    elem.parent = null;
                }
            }
            contents.remove(name);
            return elem;
        });
    }

    public <T extends FsNode> T take(T obj) {
        return readwrite(() -> {
                Map.Entry<String, FsNode> entry = contents
                    .entrySet()
                    .stream()
                    .filter(e -> e.getValue() == obj)
                    .findAny().orElseGet(() -> null);

            if (entry != null) {
                FsNode elem = entry.getValue();
                if (elem.parent == this) {
                    elem.parent = null;
                }
                contents.remove(entry.getKey());
                return (T) entry.getValue();
            } else {
                return null;
            }
        });
    }

    public FsNode getByPathChunks(List<String> path) {
        if (path.isEmpty()) {
            return this;
        }

        String firstChunk = path.get(path.size() - 1);
        path.remove(path.size()-1);

        if (firstChunk.isEmpty()) {
            return rootDirectory().getByPathChunks(path);
        }
        if (firstChunk.equals(".")) {
            if (path.isEmpty()) {
                return this;
            } else {
                return getByPathChunks(path);
            }
        }
        if (firstChunk.equals("..")) {
            return getParent().getByPathChunks(path);
        }
        if (path.isEmpty()) {
            return contents.get(firstChunk);
        }

        return readonly(() -> contents.get(firstChunk)).asDir().getByPathChunks(path);
    }

    public FsNode getByPath(String path) {
        return getByPathChunks(splitPath(path));
    }

    public void putByPath(String path, FsNode elem) {
        elem.readwrite(() -> {
            elem.parent = this;
            elem.moveTo(path);
        });
    }

    @Override
    protected String debugPrint() {
        return readonly(() -> {
                return contents.entrySet()
                    .stream()
                    .map(e -> {
                        if (e.getValue() instanceof Directory) {
                            return e.getKey() + ":\n" + e.getValue().show(1);
                        } else {
                            return e.getKey() + ": " + e.getValue().show(0);
                        }
                    })
                    .collect(Collectors.joining("\n"));
        });
    }
}
